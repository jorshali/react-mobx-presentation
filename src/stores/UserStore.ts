import { observable, action, computed } from 'mobx';

import { User } from '../models/User';

export class UserStore {
  @observable users: User[] = [];
  @observable currentUser: User;

  constructor() {
    this.currentUser = this.emptyUser();
  }

  @computed public get suggestedUsername() {
    if (this.currentUser.firstName && this.currentUser.lastName) {
      return this.currentUser.firstName.toLowerCase() + '.' + this.currentUser.lastName.toLowerCase();
    }

    return null;
  }

  @action public saveOrUpdate() {
    const newUser = Object.assign(new User(), this.currentUser);

    if (!this.currentUser.id) {
      newUser.id = this.users.length + 1;
      this.users.push(newUser);
    } else {
      let index = this.users.findIndex((u) => u.id === this.currentUser.id);
      this.users[index] = newUser;
    }

    this.currentUser = this.emptyUser();
  }

  @action public selectUser(user: User) {
    this.currentUser = {
      id: user.id,
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email
    };
  }

  @action public removeUser(removeUser: User) {
    this.users = this.users.filter(user => removeUser !== user);
  }

  private emptyUser(): User {
    return {
      id: null,
      username: '',
      firstName: '',
      lastName: '',
      email: ''
    };
  }
}