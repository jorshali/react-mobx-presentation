
import * as React from 'react';

import { Table, Panel, Glyphicon, Button } from 'react-bootstrap';
import { observer } from 'mobx-react';

import { User } from '../models/User';

interface UserListProps {
  users: User[];
  selectUser: (user: User) => void;
  removeUser: (user: User) => void;
}

@observer
export class UserList extends React.Component<UserListProps, {}> {
  render() {
    const { users, selectUser, removeUser } = this.props;
    
    if (users.length > 0) {
      return (
        <Panel>
          <Table id="employeeResults" striped={true} condensed={true} hover={true}>
            <thead>
              <tr>
                <th style={{width: '20%'}}>First Name</th>
                <th style={{width: '20%'}}>Last Name</th>
                <th style={{width: '20%'}}>Username</th>
                <th style={{width: '20%'}}>Email</th>
                <th style={{width: '20%'}}>Action</th>
              </tr>
            </thead>
            <tbody>
              {
                users.map((user: User, index: number) => {
                  return (
                    <tr key={'user-' + index}>
                      <td>{user.firstName}</td>
                      <td>{user.lastName}</td>
                      <td>{user.username}</td>
                      <td>{user.email}</td>
                      <td>
                        <Button bsSize="xsmall" bsStyle="default" onClick={() => selectUser(user)}>
                          <Glyphicon glyph="edit" /> Edit
                        </Button>
                        {' '}
                        <Button bsSize="xsmall" bsStyle="danger" onClick={() => removeUser(user)}>
                          <Glyphicon glyph="remove" /> Remove
                        </Button>
                      </td>
                    </tr>
                  );
                })
              }
            </tbody>
          </Table>
        </Panel>
      );
    }

    return null;
  }
}