import * as React from 'react';

import { Button, Form, FormGroup, FormControl, ControlLabel, Well } from 'react-bootstrap';
import { observer } from 'mobx-react';

import { User } from '../models/User';

interface UserFormProps {
  user: User;
  suggestedUsername: string;
  saveOrUpdate: () => void;
}

@observer
export class UserForm extends React.Component<UserFormProps, {}> {
  handleInputChange = (event: any) => {
    this.props.user[event.target.name] = event.target.value;
  }

  handleSaveOrUpdate = (event: any) => {
    event.preventDefault();
    this.props.saveOrUpdate();
  }

  render() {
    const { user, suggestedUsername } = this.props;

    return (
      <Well bsSize="small">
        <Form className="form">
          <FormGroup key="firstNameGroup">
            <ControlLabel>First Name</ControlLabel>
            <FormControl
              name="firstName"
              onChange={this.handleInputChange}
              placeholder="Enter First Name"
              type="text"
              value={user.firstName}
            />
          </FormGroup>
          <FormGroup key="lastNameGroup">
            <ControlLabel>Last Name</ControlLabel>
            <FormControl 
              name="lastName"
              onChange={this.handleInputChange}
              placeholder="Enter Last Name"
              type="text"
              value={user.lastName}
            />
          </FormGroup>
          <FormGroup key="emailGroup">
            <ControlLabel>Email</ControlLabel>
            <FormControl 
              name="email"
              onChange={this.handleInputChange}
              placeholder="Enter Email"
              type="text"
              value={user.email}
            />
          </FormGroup>
          <FormGroup key="usernameGroup">
            <ControlLabel>
              Username
            </ControlLabel>
            <FormControl 
              name="username"
              onChange={this.handleInputChange}
              placeholder="Enter Username"
              type="text"
              value={user.username}
            />
            <em>{suggestedUsername ? 'Suggested: ' + suggestedUsername : null}</em>
          </FormGroup>

          <Button type="submit" onClick={this.handleSaveOrUpdate} bsStyle="primary">
            {user.id ? 'Update' : 'Add'}
          </Button>
        </Form>
      </Well>
    );
  }
}