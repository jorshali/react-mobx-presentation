import * as React from 'react';
import './App.css';

import { UserForm } from './components/UserForm';
import { UserList } from './components/UserList';
import { UserStore } from './stores/UserStore';
import { inject, observer } from 'mobx-react';

const logo = require('./logo.svg');

interface AppProps {
  userStore?: UserStore;
}

@inject('userStore')
@observer
class App extends React.Component<AppProps, {}> {
  render() {
    const { userStore } = this.props;
  
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">User Management React and MobX Example</h1>
        </header>

        <div className="App-content">
          <UserForm
            user={userStore.currentUser}
            suggestedUsername={userStore.suggestedUsername}
            saveOrUpdate={() => userStore.saveOrUpdate()}
          />
          <UserList
            users={userStore.users}
            selectUser={(user) => userStore.selectUser(user)}
            removeUser={(user) => userStore.removeUser(user)}
          />
        </div>
      </div>
    );
  }
}

export default App;
